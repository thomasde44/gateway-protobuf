import unittest
from gatekeeper import limiter
import requests


class test_limiter(unittest.TestCase):

    def setUp(self):
        self.limiter_ut = limiter.limiter(1, 10)

    def test_interval_update(self):
        self.limiter_ut.update_request_interval_(10)
        self.assertEqual(self.limiter_ut.request_interval_, 10, "should be 10")

    def test_interval_update(self):
        self.limiter_ut.update_request_interval_(1000)
        self.assertEqual(self.limiter_ut.request_interval_, 1000, "should be 1000")

    def test_interval_update(self):
        self.limiter_ut.update_request_interval_(20)
        self.assertEqual(self.limiter_ut.request_interval_, 20, "should be 20")

    def test_connections_update(self):
        self.limiter_ut.update_requests_allowed_(10)
        self.assertEqual(self.limiter_ut.requests_allowed_, 10, "should be 10")

    def test_connections_update(self):
        self.limiter_ut.update_requests_allowed_(500)
        self.assertEqual(self.limiter_ut.requests_allowed_, 500, "should be 500")

    def test_connections_update(self):
        self.limiter_ut.update_requests_allowed_(5)
        self.assertEqual(self.limiter_ut.requests_allowed_, 5, "should be 10")

    def test_many_connections(self):
        tmp = limiter.limiter(5, 10)
        r1 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(r1.ok, True, "should be true")
        r2 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(r2.ok, True, "should be true")
        r3 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(r3.ok, True, "should be true")
        r4 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(r4.ok, True, "should be true")
        r5 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(tmp.is_allowed_(), False, "should be false")
        r6 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(r6.ok, True, "should be true")
        r7 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(r7.ok, True, "should be true")
        r8 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(r8.ok, True, "should be true")
        r9 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(tmp.is_allowed_(), True, "should be true")
        r10 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(r10.ok, True, "should be true")
        r11 = tmp.request(requests.get, 'https://google.com')
        self.assertEqual(r11.ok, True, "should be true")


if __name__ == '__main__':
    unittest.main()

