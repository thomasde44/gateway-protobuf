# About

Project description section.

# Installation

`python3 -m pip install .`

Traditional install on your local file system.

`python3 -m pip install -e .`

Installs using the current working directory. Very useful for development.
No need to re-install to pick up local source file changes.

# Usage

Description of API/usage of this script here.

# Releasing

Be sure to update the version in the [setup.cfg](./setup.cfg) file prior to building a new release. Please see the [semantic versioning](https://semver.org/) website for guidelines on how to version this project.

Please see [this page](https://packaging.python.org/tutorials/packaging-projects/#generating-distribution-archives) for instructions on how to build a release.

# Links

TBD
