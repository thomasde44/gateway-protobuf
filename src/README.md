# Project Sources

This folder should contain a single directory which will represent the python package name.

Please see the [PEP standard documentation](https://www.python.org/dev/peps/pep-0008/#package-and-module-names) for package naming guidelines.

The package directory must contain an `__init__.py` file within it.
