import serde
import limiter
from elasticsearch import Elasticsearch

class gateway:

    def __init__(self):
        MAX_TIMEOUT = 1
        CONNECTIONS = 1
        self.limiter_ = limiter.limiter(CONNECTIONS, MAX_TIMEOUT)
        
    def query(self, search):
        """ query takes a formatted elastic search type query and submits it to the gateway. 
            It takes the reponse and finds the payload, deserializes it and returns the 
            proto objects.
        Args:
            search: 
                elasticsearch formatted query
        """
        result = []
        srch =  self.limiter_.request(es.search, index="m7-messages-*", body=search) 
        for i in srch['hits']['hits']:
            tmp = bytes.fromhex(i['_source']['payload'])
            proto = serde.deserialize_(i['_source']['resource'], tmp)
            result.append(proto)
        return result
    
    
    def wait_for(self, search, timeout):
        """ wait_for is used to make a query that is time limited by the timeout you specify. 
            It is the present time plus the timoeout amount. It searches back office messages
            that were recevied in that time frame.
        Args:
            search: 
                is dictionary type query for elastic search. It must have the basic structure:
                    "query": {
                        "bool": {
                            "must": [  {{more search params}}  ]
                        }
                    }
            timeout:
                integer value of seconds for the timeout 
        Ex:
            search = {
                    "query": {
                        "bool": {
                                    "must": [
                                        {"match": {"your: match"}},
                                        {"match": {"your: match"}}
                                    ]
                                }
                            }
                        }
            wait_for(search, 6000)
        """
        result = []
        add = {
            "timestamp": {
            "gte" : "now/s",
            }
        }
        add['timestamp']['lte'] = 'now+'+str(timeout)+'s/s'
        search['query']['bool']['must'].append(add)
        timeout_time = time.time() + timeout
        while(timeout_time > time.time()):
            srch = self.limiter_.request(es.search, index="your-elasticsearch-index-*", body=search)
            if srch['hits']['total'] != 0:
                for i in srch['hits']['hits']:
                    tmp = bytes.fromhex(i['_source']['payload'])
                    proto = serde.deserialize_(i['_source']['resource'], tmp)
                    result.append(proto)
                return result
        return None

