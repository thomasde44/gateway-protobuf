import time
import concurrent.futures

class limiter:
    """class to help structure gateway queries for m7 data
    
    Args:
        allowed: number of connections we want to allow
        interval: the time interval that can permit the allowed 
                  number of connections
    """
    def __init__(self, allowed, interval=None):
        if allowed < 1:
            raise Exception("wrong allowed param")
        if interval == None:
            interval = 0

        self.connections_ = 0
        self.time_created_ = time.time()
        self.request_interval_ = interval
        self.requests_allowed_ = allowed


    def request_(self, function, *args, **kwargs):
        """ Request is meant to be the accessor to the gateway. It doesn't necesarily have to 
            be for the gateway it can be used to limit any function execution. If you have a 
            time period where you only want so many function to be executed you can pass them through
            the limiter class request function. It will keep track of the time and how many functions
            have been called.

        Args:
            function: the name of the function you want to execute without the call braces ()
            **kwargs: whatever paramters that would be inside the call braces of the function
                    you want to execute
        
        Ex: request_(es.search, index=None, body={'query': {"match_all": {}}})
        """
        res = None
        if time.time() < (self.time_created_ + self.request_interval_):
            if self.connections_ < self.requests_allowed_:
                self.increment_connections_()
                res = function(*args, **kwargs)

            else:
                # timer sleep
                if self.connections_ >= self.requests_allowed_:
                    time.sleep((self.time_created_ + self.request_interval_) - time.time())
                self.reset_connections_()
                self.reset_initial_time_()
                self.increment_connections_()
                res = function(*args, **kwargs)

        if time.time() > (self.time_created_ + self.request_interval_):
            self.reset_connections_()
            self.reset_initial_time_()
        return res
    
   
    def request(self, function, *kwargs):
        """
        wrap request to be async
        """
        with concurrent.futures.ThreadPoolExecutor() as executor:
            future = executor.submit(self.request_, function, *kwargs)
            return_value = future.result()
            while (not future.done()):
                continue
            return return_value
  
    def increment_connections_(self):
        """increment connections to the limiter class value by + 1"""
        self.connections_ += 1



    def reset_initial_time_(self):
        """reset beginning of time interval to the present time"""
        self.time_created_ = time.time()

    

    def reset_connections_(self):
        """reset current connections to zero"""
        self.connections_ = 0



    def update_request_interval_(self, length):
        """update the limiter class attribute for interval length"""
        self.request_interval_ = length

    

    def update_requests_allowed_(self, max):
        """update the limiter class attribute for interval max requests in the interval"""
        self.requests_allowed_ = max


    def is_allowed_(self):
        """true or false if the limiter is in a state to accept a request"""
        if time.time() < (self.time_created_ + self.request_interval_) and self.connections_ < self.requests_allowed_:
            return True
        else:
            return False

