import google.protobuf.message 



def deserialize(message_uri, byte_array):
    """ Takes in python bytes string of a proto message 
        and unmarshals it into the proto object
    
    Args:
        message_uri is the integer number for the corresponding message from our specs
        byte_array is serialized bytes of a proto message 
    Ex:
        deserialize('0', bytes)
    """
    proto_msg_types = {
                       'message dictionary types deleted'
                       }
    proto_msg_types[message_uri].ParseFromString(byte_array)
    return proto_msg_types[message_uri]
